all: build run

build:
	c++ main.cpp editor.cpp -o editor -I. `pkg-config --cflags --libs gtk+-3.0`

run:
	./editor
