#include <editor.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

string read_file(string filename) {
	ifstream file(filename);
	
	ostringstream buf;
	
	buf << file.rdbuf();
	
	return buf.str();
}

void write_file(string filename, string contents) {
	ofstream file(filename);
	
	file << contents;
}

GtkTextBuffer *string_to_text_buffer(string str) {
	GtkTextBuffer *text_buffer = gtk_text_buffer_new(NULL);
	
	gtk_text_buffer_set_text(text_buffer, str.c_str(), -1);
	
	return text_buffer;
}

string text_buffer_to_string(GtkTextBuffer *text_buffer) {
	GtkTextIter start, end;
	
	gtk_text_buffer_get_start_iter(text_buffer, &start);
	gtk_text_buffer_get_end_iter(text_buffer, &end);
	
	return gtk_text_buffer_get_text(text_buffer, &start, &end, true);
}

string request_edits(string text) {
	GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	GtkWidget *header_bar = gtk_header_bar_new();
	GtkWidget *save_button = gtk_button_new_with_label("Save");
	GtkWidget *scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	GtkWidget *text_view = gtk_text_view_new_with_buffer(
		string_to_text_buffer(text)
	);
	
	gtk_window_set_default_size(GTK_WINDOW(window), 600, 300);
	
	gtk_header_bar_set_title(GTK_HEADER_BAR(header_bar), "Text editor");
	
	gtk_container_add(GTK_CONTAINER(scrolled_window), text_view);
	gtk_container_add(GTK_CONTAINER(window), scrolled_window);
	gtk_header_bar_pack_start(GTK_HEADER_BAR(header_bar), save_button);
	gtk_window_set_titlebar(GTK_WINDOW(window), header_bar);
	
	g_signal_connect(
		G_OBJECT(window), "destroy",
		G_CALLBACK(gtk_main_quit), NULL
	);
	g_signal_connect(
		G_OBJECT(save_button), "clicked",
		G_CALLBACK(gtk_main_quit), NULL
	);
	
	gtk_widget_show_all(window);
	
	gtk_main();
	
	return text_buffer_to_string(
		gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view))
	);
}

void edit_file(char *filename) {
	write_file(filename, request_edits(read_file(filename)));
}
